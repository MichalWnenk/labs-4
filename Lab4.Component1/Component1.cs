﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Component1 : AbstractComponent, InterfaceContract
    {
        InterfaceContract interfaceContract;

        public Component1(InterfaceContract interfaceContract)
        {
            this.interfaceContract = interfaceContract;
            RegisterProvidedInterface<InterfaceContract>(this.interfaceContract);
        }

        public Component1()
        {
            RegisterRequiredInterface<InterfaceContract>();
        }

        public void MetodaPierwsza()
        {
            interfaceContract.MetodaPierwsza();
        }

        public void MetodaDruga()
        {
            interfaceContract.MetodaDruga();
        }

        public override void InjectInterface(Type type, object impl)
        {
        }
    }
}
