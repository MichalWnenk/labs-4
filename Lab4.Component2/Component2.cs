﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Component2 : AbstractComponent, InterfaceContract
    {
        public Component2()
        {
            RegisterProvidedInterface<InterfaceContract>(this);
        }


        delegate void Delegat(Component2 Component);

        void Delegacja(Delegat delegat)
        {
            if (delegat != null)
                delegat(this);
        }
        public void MetodaPierwsza()
        {
            Console.WriteLine("MetodaPierwsza");
        }
        public void MetodaDruga()
        {
            Console.WriteLine("MetodaDruga");
        }

        public override void InjectInterface(Type type, object impl)
        {
        }
    }
}
