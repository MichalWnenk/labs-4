﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2B : AbstractComponent, InterfaceContract
    {
        public Component2B()
        {
            RegisterProvidedInterface<InterfaceContract>(this);
        }

        delegate void Delegat(Component2B Component);

        void Delegacja(Delegat delegat)
        {
            if (delegat != null)
                delegat(this);
        }
        public void MetodaPierwsza()
        {
            Console.WriteLine("Cos jeden");
        }
        public void MetodaDruga()
        {
            Console.WriteLine("Cos dwa");
        }

        public override void InjectInterface(Type type, object impl)
        {
        }
    }
}
